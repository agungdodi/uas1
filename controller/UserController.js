const config = require('../config/config.js')
const bcr = require('bcryptjs');
const jwt = require('jsonwebtoken');
const userService = require('../service/UserService.js')
const User = require('../model/User.js');

module.exports.root = async (req, res) => {

    res.status(200).send("welcome.... ");
}

module.exports.login = async (req, res) => {

    const {name, password} = req.body

    if(name === null || name === "" || password === null || password === "")
        return res.status(200).send("name or password cannot be empty");
    else {

        let user = await userService.getByName(name);

        let same = await bcr.compare(password, user.password);
        if(same) {
            const token = jwt.sign({user_id: user.id, name}, config.jwt_token_secret, {expiresIn: config.jwt_expirate});

            return res.status(200).send({success:true, name: name, token: token});
        }
        else 
            return res.status(200).send({success:false, message: "login failed."});
    }
}

module.exports.getAllUsers = async (req, res) => {

    try {

        let users = await userService.getAll()
        res.status(200).send({users:users});
    } 
    catch (err) {
        res.status(200).send("Ops, there is something wrong with the service, please try to re login");
    } 

}

module.exports.create = async (req, res) => {

    const {name, password, enabled} = req.body

    if(name === null || name === "" || password === null || password === "")
        return res.status(200).send("name or password cannot be empty");
    else {

        try{

            let user = new User();
            user.name = name;
            user.password = await bcr.hash(password, 10);
            user.enabled = enabled

            userService.create(user);

            user.password = 'hidden for security reason';

            return res.status(200).send({success:true, user:user});
        }
        catch(err) {
            return res.status(200).send({success:false, message: err.message});
        }
    }
}

module.exports.deleteByName = async (req, res) => {

    const {name} = req.body
    if(name === null || name === '') return res.status(200).send({success:false, message:'parameter name cannot be empty!'})

    try {

        await userService.deleteByName(name);
        return res.status(200).send({success:true, message:'user '+name+' successfully removed!'});
    }
    catch(err) {
        return res.status(200).send({success:false, message: err.message});
    }
}