const config = require('../config/config.js')
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {

    if(req.path === '/login') next();
    else {

        const authorization = req.headers.authorization;

        if(authorization === null || authorization === ''){
            res.status(200).send("Unauthorize access");
        }
        else {
    
            try {
    
                let token = authorization.split(" ")[1];
                let verified = await jwt.verify(token, config.jwt_token_secret);
                if(verified) next();
            } 
            catch (err) {
                res.status(200).send("Something goes wrong, please refresh, reason ["+err.message+"]");
            } 
        }
    }
}