
const db = require('../config/connection.js')

module.exports.getById = async (id) => {
    return db.one("SELECT * FROM t_uas1_user WHERE id = $1", [id])
}

module.exports.getByName = async (name) => {
    return db.one("SELECT * FROM t_uas1_user WHERE name = $1", [name])
}

module.exports.getAll = async () => {
    return db.many("SELECT * FROM t_uas1_user ORDER BY name ASC")
}

module.exports.create = async (user) => {
    return db.none('INSERT INTO t_uas1_user(name, password, enabled) VALUES($1, $2, $3)', [user.name, user.password, user.enabled])
}

module.exports.deleteByName = async (name) => {
    return db.none('DELETE FROM t_uas1_user WHERE name = $1', [name])
}