const expect = require("chai").expect;
const service = require('../service/UserService.js');

describe("user service test", () => {
    it("getById should return valid value", async () => {
      let ujo = await service.getById(1)
      expect(ujo.name).to.equal("Ujo")
    });

    it('getByName should return valid value', async () => {
      let user = await service.getByName('Ujo')
      expect(user.name).to.equal('Ujo')
    });

    it('getAll should return valid row size', async () => {
      let users = await service.getAll()
      expect(users.length).to.equal(2)
    });
});