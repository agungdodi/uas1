module.exports = {
    db_config: {
        host: '127.0.0.1',
        port: 5432,
        database: 'uas1',
        user: 'software_seni',
        password: 'software_seni',
        max: 30 // use up to 30 connections
    },
    jwt_token_secret: "kura_kura_ninja",
    jwt_expirate: '8h',
    server_port:5000
}