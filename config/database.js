const pgp = require('pg-promise')()
const User = require('./model/User.js')
const UserService = require('./service/UserService.js')

const cn = {
    host: '127.0.0.1',
    port: 5432,
    database: 'uas1',
    user: 'software_seni',
    password: 'software_seni',
    max: 30 // use up to 30 connections
};

exports.db = pgp(cn);