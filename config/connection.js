const config = require('../config/config.js')
const pgp = require('pg-promise')();

module.exports = pgp(config.db_config);