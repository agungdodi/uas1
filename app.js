const express = require('express');
const app = express();
const config = require('./config/config.js')
const userController = require('./controller/UserController.js')
const Auth = require('./controller/AuthController.js')

app.listen(config.server_port, () => {
    console.log('Server running on localhost:5000...')
  })

app.use(express.json());
app.use(Auth);
app.get('/', userController.root)
app.post('/login', userController.login);
app.get('/users/list', userController.getAllUsers);
app.post('/users/create', userController.create);
app.delete('/users/delete-by-name', userController.deleteByName);

module.exports = app